function hideStartButtn(sel) {
	setTimeout(function(){
    		$(sel).fadeOut(450);
    	}, 180);// исчезает кнопка старта - чтобы не отвлекала
}
// счетчик игры - счет игрока
function userCount(sel, arr, c, err) { 
	//селектор
	//массив со словами
	//глобальный счетчик цикла игры
	var howMatchWords = arr.length - c;
		$(sel).html("Слоў у заданні: " + howMatchWords + "<br>" +
		 " правільных атветаў: " + c + "<br> памылак: " + err).
		fadeIn(1800).fadeOut(2700);
}

// появление слова задания - в параметрах управляемая задержка в секундах
function theTaskNow(sel, arr, c, sec) {
	//selector
	//arr with words
	//global counter
	//duration
	var ps = sec * 1000;
		$(sel).fadeIn(900);


				$(sel + " p").fadeIn(500).text(arr[c]).fadeOut(ps);

				//setTimeout(function() {
					//$(sel + " p").css("display", "none").text("СОБЕРИ СЛОВО ТУТ");
					//$(sel + " p").css('color', 'orange');
					//$(sel + " p").css("opacity", "0.0").text("СОБЕРИ СЛОВО ТУТ");

				//$(sel + " p").fadeIn(270).delay(1350).fadeOut(810);
				//}, 3600);

				setTimeout(function() {
					$(sel).fadeOut(90);
				    $("#ourWord").css('display', 'block').fadeIn(900);
				}, 3600);

				
				
}

// letters generator
function lettGen(s) {
	var str1 = "ЙЙЦУУУКЕЕЕНГШЎЎЎЎЁЁЁЁЗХ''''ФЫВААААПРОООЛЛЛДДДЖЖЖЭЭЭЭЯЯЯЯЧЧЧССССМММІІІІІТТТТЬЬЬЬББББЮЮЮЮЮ";
	var str = "";
	//var t = "";
	//for(var i = 0; i < 3; i++) {
		var rnd = Math.floor(Math.random() * str1.length);
		var rnd1 = Math.floor(Math.random() * str1.length);
		var rnd2 = Math.floor(Math.random() * str1.length);
		var rnd3 = Math.floor(Math.random() * str1.length);
		var rnd4 = Math.floor(Math.random() * str1.length);
		var rnd5 = Math.floor(Math.random() * str1.length);
		var rnd6 = Math.floor(Math.random() * str1.length);
		var rnd7 = Math.floor(Math.random() * str1.length);
		var rnd8 = Math.floor(Math.random() * str1.length);
		var rnd9 = Math.floor(Math.random() * str1.length);
		var rnd10 = Math.floor(Math.random() * str1.length);
		var rnd11 = Math.floor(Math.random() * str1.length);
		var rnd12 = Math.floor(Math.random() * str1.length);
		var rnd13 = Math.floor(Math.random() * str1.length);
		var rnd14 = Math.floor(Math.random() * str1.length);
		var rnd15 = Math.floor(Math.random() * str1.length);
		var rnd16 = Math.floor(Math.random() * str1.length);
		var rnd17 = Math.floor(Math.random() * str1.length);
		var rnd18 = Math.floor(Math.random() * str1.length);
		str = s + str1[rnd] + str1[rnd1] + str1[rnd2] + str1[rnd3] +
		str1[rnd4] + str1[rnd5] + str1[rnd6] + str1[rnd7] + str1[rnd8] +
		str1[rnd9] + str1[rnd10] + str1[rnd11] + str1[rnd12] + str1[rnd13] +
		str1[rnd14] + str1[rnd15] + str1[rnd16] + str1[rnd17] + str1[rnd18];
		
	//}
	str = str.toUpperCase();
    //var rand = Math.floor(Math.random() * str.length);
	//var myLit = str[rand];
	return str;
}
// та же функция для продолжения

function ltrR() {
	var str = "ЙЙЦУУУКЕЕЕНГШЎЎЎЎЁЁЁЁЗХ''''ФЫВААААПРОООЛЛЛДДДЖЖЖЭЭЭЭЯЯЯЯЧЧЧССССМММІІІІІТТТТЬЬЬЬББББЮЮЮЮЮ";
	var rand1 = Math.floor(Math.random() * str.length);
	var rand2 = Math.floor(Math.random() * str.length);
	var rand3 = Math.floor(Math.random() * str.length);
	var rand4 = Math.floor(Math.random() * str.length);
	var rand5 = Math.floor(Math.random() * str.length);
	var rand6 = Math.floor(Math.random() * str.length);
	var rand7 = Math.floor(Math.random() * str.length);
	var rand8 = Math.floor(Math.random() * str.length);
	var rand9 = Math.floor(Math.random() * str.length);
	var rand10 = Math.floor(Math.random() * str.length);
	var rand11 = Math.floor(Math.random() * str.length);
	var rand12 = Math.floor(Math.random() * str.length);
	var rand13 = Math.floor(Math.random() * str.length);
	var rand14 = Math.floor(Math.random() * str.length);
	var myLit = str[rand1] + str[rand2] + str[rand3] + str[rand4] +
	str[rand5] + str[rand6] + str[rand7] + str[rand8] + str[rand9] +
	str[rand10] + str[rand11] + str[rand12] + str[rand13] + str[rand14];

	return myLit;
}

function uncID() {
	var str = "qwertyuiopasdfghjklzxcvbnm";
	var randNum = Math.floor(Math.random() * 99999);
	var ourLT = Math.floor(Math.random() * str.length);
	var ourD = Math.floor(Math.random() * str.length);
	
	//var randLit = Math.floor(Math.random() * str.length);
	var ourID = str[ourLT] + str[ourD] + randNum;

	return ourID;
}

function randInt(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
  }

// letters to screen
function newLit(lit) {   

	var n = lit.length;
	//создаем див с буквой
	for(var e = 0; e < n; e++) {

		var unicId = uncID();

	var newLitera = $('<div class="litera" id=' + unicId + '></div>');
    	newLitera.css("background-color", "#196F3D");
    	newLitera.text(lit.charAt(e));
    	//определяем стартовую позицию на экране
    	newLitera.css("position", "absolute");
    	newLitera.css("top", "3vh");
    	newLitera.css("right", "3vw");
    	newLitera.css("cursor", "move");

    	$('body').append(newLitera); //добавляем в боди наш див с буквой

    		var forL = randInt(36, 1170);
    		var forT = randInt(198, 594);

    	//анимация перемещения на случайные позиции
    	newLitera.animate({'left':'-=' + forL + 'px', 'top':'+=' + forT + 'px'}, 1800);

    	}
    	
}





function readWord(thePlace, lett) {
	var topp = $(thePlace).offset().top;
	var leftt = $(thePlace).offset().left;
	var arr = [];
	//var c = $(lett).length;
	$(lett).each(function(){
		if($(this).offset().top - topp < 10) {
			arr.push($(this).attr('id'));
		}
	});
	//alert("top = " + topp + " and left = " + leftt);

var ourLitObj = [];
var myStr = [];
var ourW = "";
for(var b = 0; b < arr.length; b++) {

	var m = $('#' + arr[b]).offset().left;
    var lt = $('#' + arr[b]).text();

   // var ourLitObj = [];

    	ourLitObj[b] = {
    		lft: m,
    		txt: lt
    	}

    }

    	ourLitObj.sort(comLeft);
	//alert(ourLitObj[0].txt);

for (var i = 0; i < ourLitObj.length; i++) {
	//alert(ourLitObj[i].txt);
	myStr[i] = ourLitObj[i].txt;
 }
    ourW = myStr.join('');

//alert(ourW);
return ourW;
}

function comLeft(a, b) {
	if(a.lft > b.lft) return 1;
	if(a.lft < b.lft) return -1;

 	return 0;
}
