require 'test_helper'

class LessonsControllerTest < ActionDispatch::IntegrationTest
  test "should get rus" do
    get lessons_rus_url
    assert_response :success
  end

  test "should get bel" do
    get lessons_bel_url
    assert_response :success
  end

  test "should get eng" do
    get lessons_eng_url
    assert_response :success
  end

  test "should get rus01" do
    get lessons_rus01_url
    assert_response :success
  end

  test "should get rus02" do
    get lessons_rus02_url
    assert_response :success
  end

  test "should get rus03" do
    get lessons_rus03_url
    assert_response :success
  end

  test "should get bel01" do
    get lessons_bel01_url
    assert_response :success
  end

  test "should get bel02" do
    get lessons_bel02_url
    assert_response :success
  end

  test "should get bel03" do
    get lessons_bel03_url
    assert_response :success
  end

  test "should get eng01" do
    get lessons_eng01_url
    assert_response :success
  end

  test "should get eng02" do
    get lessons_eng02_url
    assert_response :success
  end

  test "should get eng03" do
    get lessons_eng03_url
    assert_response :success
  end

end
