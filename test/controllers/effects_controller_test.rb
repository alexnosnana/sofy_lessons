require 'test_helper'

class EffectsControllerTest < ActionDispatch::IntegrationTest
  test "should get eff01" do
    get effects_eff01_url
    assert_response :success
  end

  test "should get eff02" do
    get effects_eff02_url
    assert_response :success
  end

  test "should get eff03" do
    get effects_eff03_url
    assert_response :success
  end

  test "should get eff04" do
    get effects_eff04_url
    assert_response :success
  end

  test "should get eff05" do
    get effects_eff05_url
    assert_response :success
  end

end
