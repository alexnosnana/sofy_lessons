Rails.application.routes.draw do
  get 'eff_page/pageofeffects'

  get 'eff_page/effects-page'

  get 'effects/eff01'

  get 'effects/eff02'

  get 'effects/eff03'

  get 'effects/eff04'

  get 'effects/eff05'

  get 'lessons/rus'

  get 'lessons/bel'

  get 'lessons/eng'

  get 'lessons/rus01'

  get 'lessons/rus02'

  get 'lessons/rus03'

  get 'lessons/bel01'

  get 'lessons/bel02'

  get 'lessons/bel03'

  get 'lessons/eng01'

  get 'lessons/eng02'

  get 'lessons/eng03'

  get 'welcome/index'
  
  root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
